import io
import os
from argparse import ArgumentParser
import glog as log
import MySQLdb

# Imports the Google Cloud client library
from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types

_FILE_LENGTH=15  # 15 seconds


def parse_arguments():
    parser = ArgumentParser('Script to write audio transcripts to SQL database.')
    parser.add_argument('--audio_dir_path', '-a', type=str,
                        help='Path to directory containing all audio files to be written to database.')
    parser.add_argument('--debug_mode', action='store_true',
                        help='Set debugging mode on for logging.')
    return parser.parse_args()


if __name__ == '__main__':

    args = parse_arguments()

    if args.debug_mode:
        log.setLevel(log.DEBUG)

    # Instantiates a client
    client = speech.SpeechClient()

    conn = MySQLdb.connect(
        host='35.193.104.126',
        user='root',
        passwd='test123',
        db='notepod_test'
    )

    cursor = conn.cursor()

    audio_files = [os.path.join(args.audio_dir_path, f) for f in os.listdir(args.audio_dir_path)
                   if os.path.isfile(os.path.join(args.audio_dir_path, f)) and f.endswith('.flac')]

    num_audio_files = len(audio_files)

    for input_audio_file in audio_files:

        log.debug(input_audio_file)

        file_id = int(input_audio_file.split('/')[-2])

        file_index = int(input_audio_file.split('/')[-1][:3])

        # if file_index == (num_audio_files-1):
        #     audio_obj = soundfile.SoundFile(input_audio_file)
        #     file_length = int(len(audio_obj)/audio_obj.samplerate)

        file_start_time = file_index*_FILE_LENGTH
        file_end_time = file_start_time + _FILE_LENGTH

        # Loads the audio into memory
        with io.open(input_audio_file, 'rb') as audio_file:
            content = audio_file.read()
            audio = types.RecognitionAudio(content=content)

        config = types.RecognitionConfig(
            language_code='en-US'
        )

        # config = types.RecognitionConfig(
        #     encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
        #     sample_rate_hertz=44100,
        #     language_code='en-US')

        # Detects speech in the audio file
        response = client.recognize(config, audio)

        transcript = ''

        for result in response.results:
            transcript += result.alternatives[0].transcript + ' '

        log.debug('Transcript: %s' % transcript)

        cursor.execute('INSERT INTO globaltranscription(AudioId,StartOffset,EndOffset,Text) VALUES (%d,%d,%d,"%s")'
                       % (file_id, file_start_time, file_end_time, transcript))
        conn.commit()

    conn.close()
