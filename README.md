# API for transcribing English audio clips using Google Speech to Text

## Starts a server that serves requests to return text notes given end time and length of audio recording.
```
python start_note_recorder_api_server.py
```
