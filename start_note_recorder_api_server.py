#!/usr/bin/env python

"""Usage: python start_note_recorder_api_server.py
Starts a server that serves requests to return text notes given end time and length of audio recording.
"""

import glog as log
import tornado.ioloop
import tornado.web
from argparse import ArgumentParser
import note_recorder_lib

class NoteRecorderHandler(tornado.web.RequestHandler):
    """
    Tornado's handler for recording text notes.
    """

    def get(self):
        """
        Get request handler.
        :return: None
        """
        audio_id = None
        end_time = None
        audio_length = None
        try:
            audio_id = self.get_argument('audioid')
            end_time = int(self.get_argument('endtime'))
            audio_length = int(self.get_argument('length'))
        except tornado.web.MissingArgumentError as arg_error:
            log.info(arg_error)
        except ValueError as val_error:
            log.info(val_error)

        note = note_recorder_lib.get_note('/Users/krishperumal/notepod.db',
                                          audio_id=audio_id,
                                          end_time=end_time,
                                          audio_length=audio_length)

        self.write(note)


def make_app():
    """
    Tornado application handler(s).
    :return: tornado.web.Application with all the handler(s)
    """
    return tornado.web.Application([
        (r'/get/note', NoteRecorderHandler)
    ])


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument('--port', '-p', type=int, default=8888,
                        help='Port number on which the application will be served.')
    parser.add_argument('--debug_mode', action='store_true', default=False,
                        help='Switch on logging of debug messages.')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_arguments()
    app = make_app()
    app.listen(args.port)
    if args.debug_mode:
        log.setLevel(log.DEBUG)
    log.info('Tornado server running on port: %d' % args.port)
    tornado.ioloop.IOLoop.current().start()
