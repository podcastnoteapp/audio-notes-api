#!/usr/bin/env bash

# convert to FLAC file
ffmpeg -i $1 $1_output.flac

# convert to single channel audio file
ffmpeg -i $1_output.flac -ac $1_output_mono.flac

# split into multiple files of 15 seconds each
ffmpeg -i $1_output_mono.flac -f segment -segment_time 15 -c copy $1_out%03d.mp3
