import sqlite3
from nltk import word_tokenize


def get_note(path_to_db, audio_id, end_time, audio_length):
    conn = sqlite3.connect(path_to_db)
    cursor = conn.cursor()

    start_time = end_time-audio_length

    # get the two possible start offsets from the database (they both may be the same)
    # start offset is guaranteed to always be multiples of 60
    # end offset will not be so for the last audio clip in the table that does not have a full minute of audio

    search_offset1 = 60 * int(start_time / 60)
    search_offset2 = 60 * int(end_time / 60)

    # if the whole note is present in the same row
    if search_offset1 == search_offset2:
        cursor.execute(
            'SELECT text, endOffset FROM GlobalTranscription '
            'WHERE audio=%s AND startOffset=%d' % (audio_id, search_offset1))

        res = cursor.fetchone()
        transcript = res[0]
        end_offset = int(res[1])
        curr_audio_length = end_offset-search_offset1

        tokens = word_tokenize(transcript)

        start_token_offset = int((float(start_time-search_offset1) / curr_audio_length) * len(tokens))
        end_token_offset = int((float(end_time-search_offset1) / curr_audio_length) * len(tokens))

        note = ' '.join(tokens[start_token_offset:end_token_offset+1])

    else:
        cursor.execute(
            'SELECT text, endOffset FROM GlobalTranscription '
            'WHERE audio=%s AND startOffset=%d' % (audio_id, search_offset1))

        res = cursor.fetchone()
        transcript = res[0]
        end_offset = int(res[1])
        curr_audio_length = end_offset-search_offset1

        tokens = word_tokenize(transcript)
        start_token_offset = int((float(start_time-search_offset1) / curr_audio_length) * len(tokens))

        note = ' '.join(tokens[start_token_offset:])

        cursor.execute(
            'SELECT text, endOffset FROM GlobalTranscription '
            'WHERE audio=%s AND startOffset=%d' % (audio_id, search_offset2))

        res = cursor.fetchone()
        transcript = res[0]
        end_offset = int(res[1])
        curr_audio_length = end_offset-search_offset2

        tokens = word_tokenize(transcript)
        end_token_offset = int((float(end_time-search_offset2) / curr_audio_length) * len(tokens))

        note += ' '+' '.join(tokens[:end_token_offset+1])

    return note
